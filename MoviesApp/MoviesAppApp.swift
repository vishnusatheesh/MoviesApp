//
//  MoviesAppApp.swift
//  MoviesApp
//
//  Created by Vishnu Satheesh  on 15/02/24.
//

import SwiftUI

@main
struct MoviesAppApp: App {
    var body: some Scene {
        WindowGroup {
            RootViewContainer().onAppear{
                
            }
        }
    }
}
