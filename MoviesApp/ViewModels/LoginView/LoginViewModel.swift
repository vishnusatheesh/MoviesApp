//
//  LoginViewModel.swift
//  MoviesApp
//
//  Created by Vishnu Satheesh  on 16/02/24.
//

import Foundation

class LoginViewModel: ObservableObject {
    @Published var username: String = ""
    @Published var password: String = ""
    @Published var alertMessage: String = ""
    @Published var validation: Bool = true
    
    func isUsernameValid()-> Bool {
        // minimum characters 4
        if username.count>4 {
            validation = true
            return true
        }
        else {
            validation = false
            
            return false
        }
    }
    func isPasswordValid() -> Bool {
        if password.count>4 {
            return true
        }
        else {
            validation = false
            
            return false
        }
    }
    func login()->Bool {
        if isUsernameValid() && isPasswordValid() {
            return true
        }
        else {
            setAlertMessage()
            return false
        }
    }
    func setAlertMessage() {
        if !isUsernameValid() && !isPasswordValid(){
            alertMessage = "Invalid username and password"
        }
        else if !isUsernameValid(){
            alertMessage = "Invalid username"
        }
        else if !isPasswordValid() {
            alertMessage = "Invalid password"
        }
        else {
            alertMessage = ""
        }
        
    }
}
