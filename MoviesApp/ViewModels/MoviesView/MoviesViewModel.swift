//
//  MoviesViewModel.swift
//  MoviesApp
//
//  Created by Vishnu Satheesh  on 16/02/24.
//

import Foundation
import Combine
class MoviesViewModel: ObservableObject {
    @Published var movies: [Movie] = []
    @Published var isLoading = false
    @Published var isSuccess = false
   
    var webservice = WebserviceMangaer(token: nil)
    private var cancellable : AnyCancellable?
    
    func getMovieList() {
        isLoading = true
        let popularMoviesURL = "https://api.themoviedb.org/3/movie/popular?api_key=4e0be2c22f7268edffde97481d49064a&language=en-US&page=1"
        
        guard let url = URL(string: popularMoviesURL) else {return}
        cancellable = webservice.apiRequest(url: url, resultType: MoviesData.self, httpMethodType: .get)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { [weak self] (completion) in
                switch completion {
                    
                case .finished:
                    self?.isLoading = false
                    self?.isSuccess = true
                    print("success")
                case .failure(let error):
                    print(error.localizedDescription)
                    self?.isLoading = false
                    self?.isSuccess = false
                    
                }
            }, receiveValue: { [weak self] movies in
                self?.isLoading = false
                self?.movies = movies.movies
            })
    }
}
