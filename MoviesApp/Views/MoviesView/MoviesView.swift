//
//  MoviesView.swift
//  MoviesApp
//
//  Created by Vishnu Satheesh  on 15/02/24.
//

import SwiftUI

struct MoviesView: View {
    @Binding var activeView:RootViewNames;
    @StateObject private var viewModel = MoviesViewModel()
    var body: some View {
        NavigationView{
//            List(viewModel.movies,id: \.id){ movie in
//                MoviesList(movie: movie)
//                    
//                    
//            }
            ScrollView{
                LazyVStack{
                    ForEach(viewModel.movies,id: \.id) { movie in
                        MoviesList(movie: movie)
                            .frame(height: 120)
                            .padding(10)
                    }
                }
            }
            .navigationTitle("🍟 Movies")
            .onAppear { viewModel.getMovieList() }
            if viewModel.isLoading{
                LoadingView()
            }
        }
    }
}

#Preview {
    MoviesView(activeView: .constant(.moview))
}
