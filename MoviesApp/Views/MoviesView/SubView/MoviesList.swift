//
//  MoviesList.swift
//  MoviesApp
//
//  Created by Vishnu Satheesh  on 15/02/24.
//

import SwiftUI

struct MoviesList: View {
    var baseImageUrl = "https://image.tmdb.org/t/p/w300"
    var movie: Movie
    var body: some View {
        GeometryReader { geometry in
            VStack {
                HStack(spacing: 0) {
//                    ImageView(url: baseImageUrl+(movie.posterImage ?? ""), contentMode: .fit)
//                        .frame(width: 120,height: 80)
                    AsyncImage(url: URL(string: baseImageUrl+(movie.posterImage ?? ""))){ image in
                        if let image = image.image {
                            // Display the loaded image
                            image
                                .resizable()
                                .frame(width: 90,height:120)
                                .cornerRadius(7)
                        }
                        else if image.error != nil {
                            // Display a placeholder when loading failed
                            Image(systemName: "questionmark.diamond")
                                .imageScale(.large)
                        } else {
                                  
                            ProgressView()
                        }
                    }
                    
                    VStack(alignment: .leading, spacing:10) {
                        HStack(alignment: .top) {
                            Text(movie.title ?? "")
                                .lineLimit(2)
                                .font(.system(size: 12,weight: .semibold))
                            .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 0))
                        }
                        Spacer()
                        
                        HStack(alignment: .bottom){
                            Text(movie.year ?? "")
                                .font(.system(size: 12,weight:.semibold))
                                .padding(EdgeInsets(top: 5, leading: 10, bottom: 5, trailing: 10))
                                .background(Color.borderGray)
                           
                        }
                        .padding(.leading,10)
                        
                       
                       
                    }
                    
                }
                
            }
            Spacer()
            
        }
    }
}

//#Preview {
//    MoviesList(movie: movie)
//}
