//
//  LoginView.swift
//  MoviesApp
//
//  Created by Vishnu Satheesh  on 16/02/24.
//

import SwiftUI

struct LoginView: View {
    @Binding var activeView:RootViewNames;
    @ObservedObject var viewModel: LoginViewModel = LoginViewModel()
    @State private var showingAlert = false
    var body: some View {
        VStack {
                
            Spacer()
                
            VStack {
                TextField(
                    "Username",
                        text: $viewModel.username
                )
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .padding(.top, 20)
                    
                Divider()
                    
                SecureField(
                    "Password",
                    text: $viewModel.password
                )
                .padding(.top, 20)
                    
                Divider()
            }
                
            Spacer()
                

            Button(action: {
                if viewModel.login(){
                    activeView = .moview
                }
                else if !viewModel.validation {
                    showingAlert = !viewModel.validation
                }
                    
            }, label: {
                Text("Login")
                    .font(.system(size: 24, weight: .bold, design: .default))
                    .frame(maxWidth: .infinity, maxHeight: 60)
                    .foregroundColor(Color.white)
                    .background(Color.blue)
                    .cornerRadius(10)
            })
            .alert(isPresented:$showingAlert , content: {
                Alert(title: Text("Title"), message: Text(viewModel.alertMessage), dismissButton: .default(Text("OK")))
            })
            .pressableStyle()
        }
        .padding(30)
    }
    
}

#Preview {
    LoginView(activeView: .constant(.login))
}
