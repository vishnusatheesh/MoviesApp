//
//  RootViewContainer.swift
//  MoviesApp
//
//  Created by Vishnu Satheesh  on 16/02/24.
//

import SwiftUI
enum RootViewNames{
    case login,moview
}
struct RootViewContainer: View {
    @State var viewActive:RootViewNames = .login;
    var body: some View {
        ZStack{
            switch viewActive {
            case .login:
                LoginView(activeView: $viewActive)
            case .moview:
                MoviesView(activeView: $viewActive)
            }
        }
    }
}

#Preview {
    RootViewContainer()
}
