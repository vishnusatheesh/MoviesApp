//
//  ViewExtentions.swift
//  MoviesApp
//
//  Created by Vishnu Satheesh  on 15/02/24.
//

import SwiftUI

struct ImageView:View {
    //To load image from assets or URL
    private let name: String
    private let url: String
    private let width: CGFloat
    private let height: CGFloat
    private let maxWidth: CGFloat
    private let maxHeight: CGFloat
    private let placeholder: (any View)?
    private var contentMode: ContentMode = .fit
    
    /** Usage
     -- Load image from URL (Custom placeholder)
     ImageView(url: "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg", maxWidth: 150, maxHeight: 100, placeholder: Text("Loading"))
     
     -- Load image from URL (ProgressView as placeholder)
     ImageView(url: "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg", maxWidth: 150, maxHeight: 100, placeholder: ProgressView())
     
     -- Load image from Assets
     ImageView(name: "group", width: 150, height:100, contentMode: .fit)
     **/

    init(name: String = "", url:String = "", width:CGFloat? = nil, height:CGFloat? = nil, maxWidth:CGFloat = .infinity, maxHeight:CGFloat = .infinity, placeholder: (any View)? = nil, contentMode: ContentMode) {
        self.name = name
        self.url = url
        self.width = width ?? 0.0
        self.height = height ?? 0.0
        self.maxWidth = maxWidth
        self.maxHeight = maxHeight
        self.placeholder = placeholder
        self.contentMode = contentMode
    }
    
    var body: some View {
        if !self.name.isEmpty {
            Image(name)
                .resizable()
                .aspectRatio(contentMode: contentMode)
                .frame(width: width, height: height)
        }
        else if !self.url.isEmpty {
            AsyncImage(
                url: URL(string: self.url),
                content: { image in
                    image.resizable()
                        .aspectRatio(contentMode: contentMode)
                        .frame(maxWidth: maxWidth, maxHeight: maxHeight)
                },
                placeholder: {
                    if let placeholder = placeholder {
                        AnyView(placeholder)
                    }
                }
            )
        }
    }
}
