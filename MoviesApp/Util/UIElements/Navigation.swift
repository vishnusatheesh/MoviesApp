//
//  Navigation.swift
//  MoviesApp
//
//  Created by Vishnu Satheesh  on 15/02/24.
//

import SwiftUI

struct Navigate<ActionView:View,Destination:View>:View {
    let destination: Destination
    let actionView: ActionView
    
    init(destination: Destination, @ViewBuilder actionView: ()->ActionView) {
        self.destination = destination
        self.actionView = actionView()
    }
    var body: some View {
        NavigationLink(destination: destination
            .navigationBarTitle("")
            .navigationBarHidden(true), label: {
            actionView
        })
    }
}

extension UINavigationController: UIGestureRecognizerDelegate {
    override open func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
    }
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}


