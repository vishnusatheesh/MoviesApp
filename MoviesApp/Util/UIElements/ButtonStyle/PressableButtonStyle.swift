//
//  PressableButtonStyle.swift
//  MoviesApp
//
//  Created by Vishnu Satheesh  on 15/02/24.
//

import SwiftUI

import Foundation
import SwiftUI

struct PressableButtonStyle: ButtonStyle {
    
    let scaledAmount: CGFloat
    let opacity: CGFloat
     
    init(scaledAmount: CGFloat, opacity: CGFloat) {
        self.scaledAmount = scaledAmount
        self.opacity = opacity
    }
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .scaleEffect(configuration.isPressed ? scaledAmount: 1.0)
            .opacity(configuration.isPressed ? 0.9 : 1.0)
    }
}


extension View {
    func pressableStyle(scaledAmount: CGFloat = 0.9, opacity: CGFloat = 0.9  ) -> some View {
        buttonStyle(PressableButtonStyle(scaledAmount: scaledAmount,opacity: opacity))
    }
}
