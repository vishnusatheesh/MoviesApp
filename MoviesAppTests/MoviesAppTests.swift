//
//  MoviesAppTests.swift
//  MoviesAppTests
//
//  Created by Vishnu Satheesh  on 15/02/24.
//

import XCTest
@testable import MoviesApp

final class MoviesAppTests: XCTestCase {

    private var sut: LoginViewModel!
    
    override func setUpWithError() throws {
        sut = LoginViewModel()
    }
    override func tearDownWithError() throws {
        sut = nil
    }
    func test_login_fail(){
        
        sut.username = "123"
        sut.password = "123"
        let result = sut.login()
        let expected = false
        XCTAssertEqual(result, expected)
    }
    func test_login_success(){
        
        sut.username = "12345"
        sut.password = "12345"
        let result = sut.login()
        let expected = true
        XCTAssertEqual(result, expected)
    }
    func test_alert_message_both_invalid() {
       
        sut.username = "123"
        sut.password = "123"
        sut.setAlertMessage()
        let message = sut.alertMessage
        let expecedMessage = "Invalid username and password"
        XCTAssertEqual(message, expecedMessage)
        
    }
    func test_alert_message_user_invalid() {
        
        sut.username = "123"
        sut.password = "12345"
        sut.setAlertMessage()
        let message = sut.alertMessage
        let expecedMessage = "Invalid username"
        XCTAssertEqual(message, expecedMessage)
        
    }
    func test_alert_message_password_invalid() {
       
        sut.username = "12345"
        sut.password = "1234"
        sut.setAlertMessage()
        let message = sut.alertMessage
        let expecedMessage = "Invalid password"
        XCTAssertEqual(message, expecedMessage)
        
    }
    
    
    
   
}
