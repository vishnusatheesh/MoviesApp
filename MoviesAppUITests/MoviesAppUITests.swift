//
//  MoviesAppUITests.swift
//  MoviesAppUITests
//
//  Created by Vishnu Satheesh  on 15/02/24.
//

import XCTest

final class MoviesAppUITests: XCTestCase {
    private var app: XCUIApplication!
   
    override func setUpWithError() throws {
         app = XCUIApplication()
    }
    
    override func tearDownWithError() throws {
        app = nil
    }
    
    func test_login_user_textfiled() {
      // checking username textfields existance and the value check
        app.launch()
        let userTextField = app.textFields["Username"]
        XCTAssertTrue(userTextField.waitForExistence(timeout: 1))
        userTextField.tap()
        userTextField.typeText("1234")
        
        XCTAssertEqual(userTextField.value as! String, "1234")
        
        
        
    }
    func test_login_password_textfiled() {
    // checking password textfilds existence
        app.launch()
        let passwordTextField = app.secureTextFields["Password"]
        XCTAssertTrue(passwordTextField.waitForExistence(timeout: 1))
       
    }
    func test_login_login_button() {
   // checking login button existence
        app.launch()
        let button = app.buttons["Login"]
        XCTAssertTrue(button.waitForExistence(timeout: 1))
        
    }
    
}
